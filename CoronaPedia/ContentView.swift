//
//  ContentView.swift
//  CoronaPedia
//
//  Created by Luigi Borriello on 25/03/2020.
//  Copyright © 2020 Luigi Borriello. All rights reserved.
//

import Foundation
import SwiftUI
import darkLibrary

let REGIONI_JSON = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json"
let ANDAMENTO_NAZIONALE_JSON = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-andamento-nazionale.json"

class UserSettings: ObservableObject {
    @Published var region = "Italia"
    @Published var showView: Bool = false
    @Published var showView2: Bool = false
}

var giornoScelto: Date = Date()
var indexScelto: Int = 0
var JSONScelto: Int = 0
var trovatoGiorno = false

func formatDate(_ date: Date) -> String
{
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "it-IT")
    formatter.dateStyle = .long
    return formatter.string(from: date)
}

func formatDateToJSON(_ date: Date) -> String
{
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "it-IT")
    formatter.dateFormat = "y-MM-dd"
    var str = formatter.string(from: date)
    str = "\(str)T17:00:00"
    return str
}

public struct JSON: Codable
{
    var data: String
    var totale_positivi: Int
    var variazione_totale_positivi: Int
    var dimessi_guariti: Int
    var deceduti: Int
}

var giorniNazione = [JSON]()

struct ContentView: View
{
    @EnvironmentObject var userData: UserSettings
    
    init()
    {
        loadJSON(&giorniNazione, from: URL(string: ANDAMENTO_NAZIONALE_JSON)!, debug: false)
            
        var i = 0
        var temp = false
        
        for giorno in giorniNazione
        {
            if(formatDateToJSON(Date()) == giorno.data)
            {
                temp = true
                giornoScelto = Date()
                indexScelto = i
                JSONScelto = 0
            }
            i = i + 1
        }
        
        i = 0
        
        if(temp == false)
        {
            for giorno in giorniNazione
            {
                if(formatDateToJSON(Date.yesterday) == giorno.data)
                {
                    giornoScelto = Date.yesterday
                    indexScelto = i
                    JSONScelto = 0
                }
                i = i + 1
            }
        }
    }
    
    @State var testVar: Int = 0
    
    var body: some View
    {
        NavigationView
        {
            TabView
            {
                VStack()
                {
                    VStack()
                    {
                        Text("CoronaPedia 🦠")
                        .bold()
                        .font(.largeTitle)
                        .foregroundColor(.red)
                        
                        Text("Aggiornamenti")
                        .bold()
                        .font(.subheadline)
                        
                        Divider()
                        .padding(.leading, 40)
                        .padding(.trailing, 40)
                        .onAppear()
                        {
                            if(self.userData.region != "Italia")
                            {
                                var i = 0
                                trovatoGiorno = false
                                for regione in regioni
                                {
                                    if(regione.denominazione_regione == self.userData.region)
                                    {
                                        if(formatDateToJSON(giornoScelto) == regione.data)
                                        {
                                            indexScelto = i
                                            JSONScelto = 1
                                            trovatoGiorno = true
                                        }
                                    }
                                    i = i + 1
                                }
                            }
                            else
                            {
                                var i = 0

                                trovatoGiorno = false
                                for giorno in giorniNazione
                                {
                                    if(formatDateToJSON(giornoScelto) == giorno.data)
                                    {
                                        indexScelto = i
                                        JSONScelto = 0
                                        trovatoGiorno = true
                                    }
                                
                                    i = i + 1
                                }
                            }
                            
                            self.userData.showView2 = false
                        }
                
                        Button(action:
                        {
                            self.userData.showView = true
                        })
                        {
                            Text("\(formatDate(giornoScelto))")
                            .font(.largeTitle)
                            .foregroundColor(.red)
                            .bold()
                        }
                        
                        HStack()
                        {
                            Text("in")
                            .font(.largeTitle)
                            .bold()
                            
                            Button(action:
                            {
                                self.userData.showView2 = true
                            })
                            {
                                Text("\(userData.region)")
                                .font(.largeTitle)
                                .foregroundColor(.red)
                                .bold()
                            }
                            
                            NavigationLink(destination: dateView().navigationBarTitle("Seleziona la data", displayMode: .inline), isActive: $userData.showView)
                            {
                                EmptyView()
                            }
                            
                            NavigationLink(destination: regionView().navigationBarTitle("Seleziona la regione", displayMode: .inline), isActive: $userData.showView2)
                            {
                                EmptyView()
                            }
                        }
                    }
                    .padding(.all, 60)
                    
                    VStack(alignment: .center)
                    {
                        if(trovatoGiorno == false)
                        {
                            Text("Dati non trovati!")
                            .font(.largeTitle)
                            .foregroundColor(.red)
                            .bold()
                            .underline()
                        }
                        else
                        {
                        
                            Text("Contagiati totali:")
                            .font(.headline)
                                
                            HStack(alignment: .center)
                            {
                                if(JSONScelto == 0)
                                {
                                    Text("\(giorniNazione[indexScelto].totale_positivi)")
                                    .font(.largeTitle)
                                    .foregroundColor(.red)
                                    .bold()
                                    .underline()
                                    
                                    giorniNazione[indexScelto].variazione_totale_positivi > 0 ?
                                    (
                                        Text("+\(giorniNazione[indexScelto].variazione_totale_positivi)")
                                        .font(.headline)
                                        .foregroundColor(.red)
                                    ) :
                                    (
                                        Text("\(giorniNazione[indexScelto].variazione_totale_positivi)")
                                        .font(.headline)
                                        .foregroundColor(.green)
                                    )
                                }
                                else if(JSONScelto == 1)
                                {
                                    Text("\(regioni[indexScelto].totale_positivi)")
                                    .font(.largeTitle)
                                    .foregroundColor(.red)
                                    .bold()
                                    .underline()
                                    
                                    regioni[indexScelto].variazione_totale_positivi > 0 ?
                                    (
                                        Text("+\(regioni[indexScelto].variazione_totale_positivi)")
                                        .font(.headline)
                                        .foregroundColor(.red)
                                    ) :
                                    (
                                        Text("\(regioni[indexScelto].variazione_totale_positivi)")
                                        .font(.headline)
                                        .foregroundColor(.green)
                                    )
                                }
                            }
                            .padding(.bottom)
                            .padding(.bottom)
                                
                            Text("Deceduti totali:")
                            .font(.headline)
                                
                            HStack(alignment: .center)
                            {
                                if(JSONScelto == 0)
                                {
                                    Text("\(giorniNazione[indexScelto].deceduti)")
                                    .font(.largeTitle)
                                    .foregroundColor(.red)
                                    .bold()
                                    .underline()
                                    
                                    (giorniNazione[indexScelto].deceduti - giorniNazione[(indexScelto-1)].deceduti) > 0 ?
                                    (
                                        Text("+\((giorniNazione[indexScelto].deceduti - giorniNazione[(indexScelto-1)].deceduti))")
                                        .font(.headline)
                                        .foregroundColor(.red)
                                    ) :
                                    (
                                        Text("\((giorniNazione[indexScelto].deceduti - giorniNazione[(indexScelto-1)].deceduti))")
                                        .font(.headline)
                                        .foregroundColor(.green)
                                    )
                                }
                                else if(JSONScelto == 1)
                                {
                                    Text("\(regioni[indexScelto].deceduti)")
                                    .font(.largeTitle)
                                    .foregroundColor(.red)
                                    .bold()
                                    .underline()
                                    
                                    (regioni[indexScelto].deceduti - regioni[(indexScelto-21)].deceduti) > 0 ?
                                    (
                                        Text("+\((regioni[indexScelto].deceduti - regioni[(indexScelto-21)].deceduti))")
                                        .font(.headline)
                                        .foregroundColor(.red)
                                    ) :
                                    (
                                        Text("\((regioni[indexScelto].deceduti - regioni[(indexScelto-21)].deceduti))")
                                        .font(.headline)
                                        .foregroundColor(.green)
                                    )
                                }
                            }
                            .padding(.bottom)
                            .padding(.bottom)
                            
                            Text("Guariti totali:")
                            .font(.headline)
                                
                            HStack(alignment: .center)
                            {
                                if(JSONScelto == 0)
                                {
                                    Text("\(giorniNazione[indexScelto].dimessi_guariti)")
                                    .font(.largeTitle)
                                    .foregroundColor(.red)
                                    .bold()
                                    .underline()
                                    
                                    (giorniNazione[indexScelto].dimessi_guariti - giorniNazione[(indexScelto-1)].dimessi_guariti) > 0 ?
                                    (
                                        Text("+\((giorniNazione[indexScelto].dimessi_guariti - giorniNazione[(indexScelto-1)].dimessi_guariti))")
                                        .font(.headline)
                                        .foregroundColor(.red)
                                    ) :
                                    (
                                        Text("\((giorniNazione[indexScelto].dimessi_guariti - giorniNazione[(indexScelto-1)].dimessi_guariti))")
                                        .font(.headline)
                                        .foregroundColor(.green)
                                    )
                                }
                                else if(JSONScelto == 1)
                                {
                                    Text("\(regioni[indexScelto].dimessi_guariti)")
                                    .font(.largeTitle)
                                    .foregroundColor(.red)
                                    .bold()
                                    .underline()
                                    
                                    (regioni[indexScelto].dimessi_guariti - regioni[(indexScelto-21)].dimessi_guariti) > 0 ?
                                    (
                                        Text("+\((regioni[indexScelto].dimessi_guariti - regioni[(indexScelto-21)].dimessi_guariti))")
                                        .font(.headline)
                                        .foregroundColor(.red)
                                    ) :
                                    (
                                        Text("\((regioni[indexScelto].dimessi_guariti - regioni[(indexScelto-21)].dimessi_guariti))")
                                        .font(.headline)
                                        .foregroundColor(.green)
                                    )
                                }
                            }
                        }
                    }
                    Spacer()
                }
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                .edgesIgnoringSafeArea(.all)
                    
                .tabItem
                {
                    Image(systemName: "paperplane.fill")
                    Text("Aggiornamenti")
                }
                
                VStack
                {
                    Text("CoronaPedia 🦠")
                    .bold()
                    .font(.largeTitle)
                    .foregroundColor(.red)
                    .padding(.all)
                    
                    VStack(alignment: .leading)
                    {
                        Text("L'OMS e il governo italiano prevedono una serie di accorgimenti: seguili per garantire la tua salute, dei tuoi familiari e degli altri cittadini.")
                    }
                    
                    Divider()
                    .padding(.leading, 40)
                    .padding(.trailing, 40)
                    .padding(.bottom)
                
                    Text("Collegamenti utili:")
                    .font(.title)
                    .bold()
                    .foregroundColor(.red)
                    .padding(.bottom)
                    
                    VStack(alignment: .leading)
                    {
                        Button(action:
                        {
                            let url: NSURL = URL(string: "http://www.salute.gov.it/portale/malattieInfettive/dettaglioFaqMalattieInfettive.jsp?lingua=italiano&id=228")! as NSURL
                            UIApplication.shared.open(url as URL)
                        })
                        {
                            Text("FAQ - Covid-19, domande e risposte")
                            .italic()
                        }
                        .padding(.bottom)
                    
                        Button(action:
                        {
                            let url: NSURL = URL(string: "http://www.salute.gov.it/portale/nuovocoronavirus/dettaglioContenutiNuovoCoronavirus.jsp?lingua=italiano&id=5364&area=nuovoCoronavirus&menu=vuoto")! as NSURL
                            UIApplication.shared.open(url as URL)
                        })
                        {
                            Text("Covid-19 - Numeri verdi regionali")
                            .italic()
                        }
                        .padding(.bottom)
                    
                        Button(action:
                        {
                            let url: NSURL = URL(string: "http://www.governo.it/it/articolo/decreto-iorestoacasa-domande-frequenti-sulle-misure-adottate-dal-governo/14278")! as NSURL
                            UIApplication.shared.open(url as URL)
                        })
                        {
                            Text("#IoRestoaCasa domande sulle misure adottate")
                            .italic()
                        }
                        .padding(.bottom)
                    
                        Button(action:
                        {
                            let url: NSURL = URL(string: "http://www.salute.gov.it/imgs/C_17_opuscoliPoster_340_allegato.pdf")! as NSURL
                            UIApplication.shared.open(url as URL)
                        })
                        {
                            Text("Lavati correttamente le mani")
                            .italic()
                        }
                        .padding(.bottom)
                    
                        Button(action:
                        {
                            let url: NSURL = URL(string: "https://www.interno.gov.it/sites/default/files/allegati/nuovo_modello_autodichiarazione_26.03.2020_editabile.pdf")! as NSURL
                            UIApplication.shared.open(url as URL)
                        })
                        {
                            Text("Autocertificazione spostamenti (PDF)")
                            .italic()
                        }
                        .padding(.bottom)
                    }
                    
                    Spacer()
                }
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                .edgesIgnoringSafeArea(.all)
                    
                .tabItem
                {
                    Image(systemName: "questionmark")
                    Text("Cosa fare")
                }
                .padding(.all, 40)
            }
            .accentColor(.red)
        }
        .navigationBarTitle("Indietro")
    }

    struct ContentView_Previews: PreviewProvider
    {
        static var previews: some View
        {
            ContentView()
        }
    }
}

extension Date
{
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}
