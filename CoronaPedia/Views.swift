//
//  Views.swift
//  CoronaPedia
//
//  Created by Luigi Borriello on 26/03/2020.
//  Copyright © 2020 Luigi Borriello. All rights reserved.
//

import SwiftUI
import darkLibrary

public struct JSONr: Codable
{
    var data: String
    var totale_positivi: Int
    var variazione_totale_positivi: Int
    var dimessi_guariti: Int
    var deceduti: Int
    var denominazione_regione: String
}

var regioni = [JSONr]()

struct dateView: View
{
    @EnvironmentObject var dataUser: UserSettings
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    struct DatePicker2: UIViewRepresentable
    {
        @Binding var date: Date

        private let datePicker = UIDatePicker()

        func makeUIView(context: Context) -> UIDatePicker
        {
            datePicker.datePickerMode = .date
            datePicker.locale = Locale(identifier: "it-IT")
            datePicker.addTarget(context.coordinator, action: #selector(Coordinator.changed(_:)), for: .valueChanged)
            return datePicker
        }

        func updateUIView(_ uiView: UIDatePicker, context: Context)
        {
            datePicker.date = date
        }

        func makeCoordinator() -> DatePicker2.Coordinator
        {
            Coordinator(date: $date)
        }

        class Coordinator: NSObject
        {
            private let date: Binding<Date>

            init(date: Binding<Date>)
            {
                self.date = date
            }

            @objc func changed(_ sender: UIDatePicker)
            {
                self.date.wrappedValue = sender.date
                giornoScelto = self.date.wrappedValue
            }
        }
    }
    
    var btnBack: some View
    {
        Button(action:
        {
            self.dataUser.showView = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.presentationMode.wrappedValue.dismiss()
            }
        })
        {
            Text("Indietro")
        }
    }
    
    init()
    {
        UINavigationBar.appearance().tintColor = .red
    }
    
    @State var date = giornoScelto
    
    var body: some View
    {
        VStack
        {
            DatePicker2(date: $date)
            Text("\(formatDate(date))")
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .edgesIgnoringSafeArea(.all)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: btnBack)
    }
}

struct regionView: View
{
    @EnvironmentObject var dataUser: UserSettings
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    init()
    {
        loadJSON(&regioni, from: URL(string: REGIONI_JSON)!, debug: false)
    }
    
    var btnBack: some View
    {
        Button(action:
        {
            self.dataUser.showView2 = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.presentationMode.wrappedValue.dismiss()
            }
        })
        {
            Text("Indietro")
        }
    }
    
    var body: some View
    {
        VStack
        {
            Picker(selection: self.$dataUser.region, label: Text(" "))
            {
                ForEach(regioni, id: \.denominazione_regione) { regione in
                    Text("\(regione.denominazione_regione)")
                }
            }
            .labelsHidden()
            .pickerStyle(WheelPickerStyle())
            
            if(dataUser.region != "Italia")
            {
                Text("\(dataUser.region)")
                .padding(.bottom)
                
                Button(action:
                {
                    self.dataUser.showView2 = false
                    self.dataUser.region = "Italia"
                    var i = 0

                    trovatoGiorno = false
                    
                    for giorno in giorniNazione
                    {
                        if(formatDateToJSON(giornoScelto) == giorno.data)
                        {
                            indexScelto = i
                            JSONScelto = 0
                            print("Trovata data")
                            trovatoGiorno = true
                        }
                    
                        i = i + 1
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.presentationMode.wrappedValue.dismiss()
                    }
                })
                {
                    Text("Reimposta Italia")
                    .foregroundColor(.red)
                }
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .edgesIgnoringSafeArea(.all)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: btnBack)
    }
}
