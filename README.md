# **CoronaPedia** - *Version* **1.0**

iOS app for checking Italian information about Coronavirus

It gets the data through JSON via Protezione Civile Italiana repository (https://github.com/pcm-dpc/COVID-19) that is updated daily.

Since it's not allowed to publish this kind of app on the App Store, I used it as a learning experience because it's my first approach using SwiftUI.

I made this app for the Apple Developer Academy challenge where we needed to make a personal app. Also, I used this app for a hackaton on devpost.com.

--------------------------------

iOS app per controllare le informazioni italiane sul Coronavirus.

Ottiene i dati tramite JSON dalla repository della Protezione Civile Italiana (https://github.com/pcm-dpc/COVID-19) che viene aggiornata ogni giorno.

Dal momento che non si può pubblicare questo tipo di app sull'App Store, ho realizzato l'app per imparare a usare meglio SwiftUI.

Ho realizzato quest'app per la challenge dell'Apple Developer Academy che era quella di realizzare un'app da soli. Inoltre, ho anche usato l'app per un hackaton su devpost.com.